const num1 = 2;
const cube = num1 ** 3;
console.log(`The cube of ${num1} is ${cube}.`);


const fullAddress = ["258", "Washington Ave NW", "California", "90011"]
const [streetNumber, street, state, zipCode] = fullAddress;
console.log(`I live at ${streetNumber} ${street}, ${state} ${zipCode}.`);


const animals = {
	animalName: "Lolong",
	animalWeight: "1075 kgs",
	animalMeasurement: "20ft 3in"
}
const {animalName, animalWeight, animalMeasurement} = animals;
console.log(`${animalName} was a saltwater crocodile. He weighed at ${animalWeight} with a measurement of ${animalMeasurement}.`);


const numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => {
	console.log(number)
});
const reduceNumber = numbers.reduce((now, next) => now + next);
console.log(reduceNumber);



class dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};
const myDog = new dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);